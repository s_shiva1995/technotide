﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class eventselect : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnform_Click(object sender, EventArgs e)
    {
        if (chckcodehunt.Checked == true)
        {
            Response.Redirect("~\\code hunt.aspx");
        }
        if (chck333.Checked == true)
        {
            Response.Redirect("~\\3x3x3.aspx");
        }
        if (chckcs.Checked == true)
        {
            Response.Redirect("~\\counter strike.aspx");
        }
        if (chckdebate.Checked == true)
        {
            Response.Redirect("~\\debate.aspx");
        }
        if (chckinteract.Checked == true)
        {
            Response.Redirect("~\\Interacting Personalities.aspx");
        }
        if (chckmw.Checked == true)
        {
            Response.Redirect("~\\nfs most wanted.aspx");
        }
        if (chckscavenger.Checked == true)
        {
            Response.Redirect("~\\Scavenger hunt.aspx");
        }
    }

}