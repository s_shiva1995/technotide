﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class success : System.Web.UI.Page
{
    SqlConnection cn;
    SqlDataReader rd;
    SqlCommand cmd;
    protected void Page_Load(object sender, EventArgs e)
    {
        string eventname=Session["event"].ToString();
        string id = Session["register"].ToString();
        cn = new SqlConnection(@"Data Source=.\SQLEXPRESS;AttachDbFilename=G:\community\technoTide\App_Data\technoTide.mdf;Integrated Security=True;User Instance=True");

        if (eventname == "Interacting Personalities")
        {
            string iduser = "select * from interacting where id='" + id + "'";
            cn.Open();
            cmd = new SqlCommand(iduser, cn);
            rd = cmd.ExecuteReader();
            while (rd.Read())
            {
                if (rd["id"].ToString() == id)
                {
                    Label1.Text = rd["id"].ToString();
                    Label2.Text = rd["name"].ToString();
                    Label3.Text = rd["fname"].ToString();
                    Label4.Text = rd["year"].ToString();
                    Label5.Text = rd["branch"].ToString();
                    Label6.Text = rd["rollno"].ToString();
                    Label7.Text = rd["mobileno"].ToString();
                    Label8.Text = rd["emailid"].ToString();
                    Label9.Text = "Interacting Personalities";
                }
            }
            rd.Close();
            cn.Close();
        }
        if (eventname == "3x3x3")
        {
            string iduser = "select * from 3x3x3 where id='" + id + "'";
            cn.Open();
            cmd = new SqlCommand(iduser, cn);
            rd = cmd.ExecuteReader();
            while (rd.Read())
            {
                if (rd["id"].ToString() == id)
                {
                    Label1.Text = rd["id"].ToString();
                    Label2.Text = rd["name"].ToString();
                    Label3.Text = rd["fname"].ToString();
                    Label4.Text = rd["year"].ToString();
                    Label5.Text = rd["branch"].ToString();
                    Label6.Text = rd["rollno"].ToString();
                    Label7.Text = rd["mobileno"].ToString();
                    Label8.Text = rd["emailid"].ToString();
                    Label9.Text = "3x3x3";
                }
            }
            rd.Close();
            cn.Close(); 
        }
        if (eventname == "debate")
        {
            string iduser = "select * from debate where id='" + id + "'";
            cn.Open();
            cmd = new SqlCommand(iduser, cn);
            rd = cmd.ExecuteReader();
            while (rd.Read())
            {
                if (rd["id"].ToString() == id)
                {
                    Label1.Text = rd["id"].ToString();
                    Label2.Text = rd["name"].ToString();
                    Label3.Text = rd["fname"].ToString();
                    Label4.Text = rd["year"].ToString();
                    Label5.Text = rd["branch"].ToString();
                    Label6.Text = rd["rollno"].ToString();
                    Label7.Text = rd["mobileno"].ToString();
                    Label8.Text = rd["emailid"].ToString();
                    Label9.Text = "Debate";
                }
            }
            rd.Close();
            cn.Close();
        }
        if (eventname == "codehunt")
        {
            string iduser = "select * from codehunt where id='" + id + "'";
            cn.Open();
            cmd = new SqlCommand(iduser, cn);
            rd = cmd.ExecuteReader();
            while (rd.Read())
            {
                if (rd["id"].ToString() == id)
                {
                    Label1.Text = rd["id"].ToString();
                    Label2.Text = rd["name"].ToString();
                    Label3.Text = rd["fname"].ToString();
                    Label4.Text = rd["year"].ToString();
                    Label5.Text = rd["branch"].ToString();
                    Label6.Text = rd["rollno"].ToString();
                    Label7.Text = rd["mobileno"].ToString();
                    Label8.Text = rd["emailid"].ToString();
                    Label9.Text = "Code Hunt";
                }
            }
            rd.Close();
            cn.Close();
        }
        if (eventname == "mostwanted")
        {
            string iduser = "select * from mostwanted where id='" + id + "'";
            cn.Open();
            cmd = new SqlCommand(iduser, cn);
            rd = cmd.ExecuteReader();
            while (rd.Read())
            {
                if (rd["id"].ToString() == id)
                {
                    Label1.Text = rd["id"].ToString();
                    Label2.Text = rd["name"].ToString();
                    Label3.Text = rd["fname"].ToString();
                    Label4.Text = rd["year"].ToString();
                    Label5.Text = rd["branch"].ToString();
                    Label6.Text = rd["rollno"].ToString();
                    Label7.Text = rd["mobileno"].ToString();
                    Label8.Text = rd["emailid"].ToString();
                    Label9.Text = "Code Hunt";
                }
            }
            rd.Close();
            cn.Close();
        }
        if (eventname == "counterstrike")
        {
            string iduser = "select * from counterstrike where id='" + id + "'";
            cn.Open();
            cmd = new SqlCommand(iduser, cn);
            rd = cmd.ExecuteReader();
            while (rd.Read())
            {
                if (rd["id"].ToString() == id)
                {
                    Label1.Text = rd["id"].ToString();
                    Label2.Text = rd["mem1"].ToString();
                    Label3.Text = rd["mem2"].ToString();
                    Label4.Text = rd["mem3"].ToString();
                    Label5.Text = rd["mem4"].ToString();
                    Label6.Text = rd["mem5"].ToString();
                    Label7.Text = rd["year"].ToString();
                    Label8.Text = rd["branch"].ToString();
                    Label9.Text = rd["rollno1"].ToString();
                    Label10.Text = rd["rollno2"].ToString();
                    Label11.Text = rd["rollno3"].ToString();
                    Label12.Text = rd["rollno4"].ToString();
                    Label14.Text = rd["rollno5"].ToString();
                    Label15.Text = rd["mobileno"].ToString();
                    Label16.Text = rd["emailid"].ToString();

                }
            }
            rd.Close();
            cn.Close();
        }
        if (eventname == "scavengerhunt")
        {
            string iduser = "select * from scavenger where id='" + id + "'";
            cn.Open();
            cmd = new SqlCommand(iduser, cn);
            rd = cmd.ExecuteReader();
            while (rd.Read())
            {
                if (rd["id"].ToString() == id)
                {
                    Label1.Text = rd["id"].ToString();
                    Label2.Text = rd["mem1"].ToString();
                    Label3.Text = rd["mem2"].ToString();
                    Label4.Text = rd["mem3"].ToString();
                    
                    Label5.Text = rd["year"].ToString();
                    Label6.Text = rd["branch"].ToString();
                    Label7.Text = rd["rollno1"].ToString();
                    Label8.Text = rd["rollno2"].ToString();
                    Label9.Text = rd["rollno3"].ToString();
                    
                    Label10.Text = rd["mobileno"].ToString();
                    Label11.Text = rd["emailid"].ToString();

                }
            }
            rd.Close();
            cn.Close();
        }
    }
    protected void home_Click(object sender, EventArgs e)
    {
        Response.Redirect("~//eventselect.aspx");
        Session.Clear();
    }
}