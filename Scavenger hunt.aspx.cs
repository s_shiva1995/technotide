﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class Scavenger_hunt : System.Web.UI.Page
{
    SqlConnection cn;
    SqlCommand cmd;
    SqlDataReader rd;
    protected void Page_Load(object sender, EventArgs e)
    {
        cn = new SqlConnection(@"Data Source=.\SQLEXPRESS;AttachDbFilename=G:\community\technoTide\App_Data\technoTide.mdf;Integrated Security=True;User Instance=True");
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        int i = 1;
        ViewState["i"] = i.ToString();
        try
        {
            string select = "select * from scavenger";
            cmd = new SqlCommand(select, cn);
            cn.Open();
            rd = cmd.ExecuteReader();
            while (rd.Read())
            {
                i = Convert.ToInt32(ViewState["i"]);
                i++;
                ViewState["i"] = i.ToString();
            }
            rd.Close();
            cn.Close();
            i = Convert.ToInt32(ViewState["i"]);
            string register = "insert into scavenger values('tTFx1SH00" + i + "','" + txtmem1.Text + "','" + txtmem2.Text + "','" + txtmem3.Text + "','" + dropyear.Text + "','" + txtbranch.Text + "','" + txtroll1.Text + "','" + txtroll2.Text + "','" + txtroll3.Text + "','" + txtmob.Text + "','" + txtemail.Text + "')";
            cmd = new SqlCommand(register, cn);
            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
            i = Convert.ToInt32(ViewState["i"]);
            Session["register"] = "tTFx1SH00" + i;
            Session["event"] = "scavengerhunt";
            Response.Redirect("~\\success.aspx");
        }
        catch
        {
        }
    }
}